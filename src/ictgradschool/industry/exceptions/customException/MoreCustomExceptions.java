package ictgradschool.industry.exceptions.customException;

import ictgradschool.Keyboard;

import static java.lang.Character.isDigit;

public class MoreCustomExceptions {

    char characters = 0;

    public void start() {
        try {
            System.out.print("Enter a string of at most 100 characters:");
            String characters = Keyboard.readInput();
            System.out.print("You entered:");
            String[] parts = characters.split(" ");


            if(characters.length()>100){
                throw new ExceedMaxStringLengthException("It is more than 100 characters");
            }
            if(isDigit(characters.charAt(0))){
                throw new InvalidWordException("It contains invalid words");
            }


            for (int i = 0; i < parts.length; i++) {
                char word = parts[i].charAt(0);

                System.out.println(word);
            }


        } catch (InvalidWordException e) {
            System.out.println(e.getMessage());

        } catch (ExceedMaxStringLengthException e) {
            System.out.println(e.getMessage());

        }


    }




    public static void main(String[] args) {
        new MoreCustomExceptions().start();
    }
}