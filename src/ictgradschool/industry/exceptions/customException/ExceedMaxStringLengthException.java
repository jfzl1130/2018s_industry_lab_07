package ictgradschool.industry.exceptions.customException;

public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException(String message) {
        super(message);
    }
}
