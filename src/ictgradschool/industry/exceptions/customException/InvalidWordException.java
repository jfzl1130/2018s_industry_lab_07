package ictgradschool.industry.exceptions.customException;

public class InvalidWordException extends Exception {
    public InvalidWordException(String message) {
        super(message);
    }
}
